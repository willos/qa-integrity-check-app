package com.amarula.integrity_check_app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.amarula.integrity_check_app.MainActivity;
import com.amarula.integrity_check_app.NotificationIntegrityCheck;

/**
 * Broadcast receiver for ACTION_BOOT_COMPLETED broadcast.
 * Contributors:
 * Peter Janicka (peterj@amarulasolutions.com)
 */

public class OnBootReceiver extends BroadcastReceiver {
    private static final String TAG = "IntegrityCheckReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Log.d(TAG, "Boot completed received");
            Intent startIntent = new Intent(context, MainActivity.class);
            startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(startIntent);
        }

    }
}
