package com.amarula.integrity_check_app;

import android.os.AsyncTask;
import android.util.Log;

import com.amarula.integrity_check_app.util.FileHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Task for checking logcat for ResetManager events. Parsed lines which contains events from
 * ResetManager are saved to file.
 * <p>
 * Contributors:
 * Peter Janicka (peterj@amarulasolutions.com)
 */

class CheckLogsTask extends AsyncTask<Void, Void, Void> {
    private static final String TAG = "CheckLogTask";

    private static final String COMMAND_LOGCAT = "logcat";
    private static final String COMMAND_LOGCAT_PARAMETER_CLEAR = "-c";
    private static final String TAG_RESET_MANAGER = "ResetManager";
    private static final String CHECK_SEPARATOR = "\n==============================================\n";
    private static final String RESET_MANAGER_UP = "ResetManager is up and running";
    private static final String RESET_MANAGER_FINISH = "ResetManager finished";

    private MainActivity mActivity;

    private BufferedReader bufferedReader;

    public CheckLogsTask(MainActivity activity) {
        mActivity = activity;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            String[] command = new String[]{COMMAND_LOGCAT};

            Process process = Runtime.getRuntime().exec(command);

            bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while (!isCancelled() && (line = bufferedReader.readLine()) != null) {
                if (line.contains(TAG_RESET_MANAGER)) {
                    parseLine(line);
                }

            }
        } catch (IOException e) {
            Log.e(TAG, "getLog failed", e);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error: ", e);
                }
            }
        }

        Log.d(TAG, "Task ending");
        return null;
    }

    @Override
    protected void onCancelled() {
        //Clear log to prevent parse the same lines multiple times on the next start
        Log.d(TAG, "Clear logcat");
        String[] command = new String[]{COMMAND_LOGCAT, COMMAND_LOGCAT_PARAMETER_CLEAR};

        try {
            Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            Log.e(TAG, "Clear log failed: ", e);
        }
        Log.d(TAG, "Cancel task");
    }

    private void parseLine(String line) {
        SimpleDateFormat s = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        String format = s.format(new Date());

        if (line.contains(RESET_MANAGER_UP)) {
            FileHelper.saveToFile(mActivity, CHECK_SEPARATOR + format + '\n' + line);
        } else if (line.contains(RESET_MANAGER_FINISH)) {
            FileHelper.saveToFile(mActivity, format + '\n' + line + CHECK_SEPARATOR);
        } else {
            FileHelper.saveToFile(mActivity, line);
        }

        mActivity.refreshList();
    }

    public void unblockIfNeeded() {
        try {
            if (bufferedReader != null)
                bufferedReader.close();
        } catch (IOException e) {
            Log.e(TAG, "Error: ", e);
        }
    }
}