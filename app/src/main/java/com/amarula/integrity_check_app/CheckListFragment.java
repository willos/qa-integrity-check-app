package com.amarula.integrity_check_app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amarula.integrity_check_app.util.FileHelper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Fragment with parsed lines from logcat. Contains list of integrity checks.
 * <p>
 * Contributors:
 * Peter Janicka (peterj@amarulasolutions.com)
 */

public class CheckListFragment extends Fragment {

    private static final ExecutorService executor = Executors.newCachedThreadPool();

    @BindView(R.id.list_checks)
    TextView mChecksList;

    private Unbinder mUnbinder;

    private class LogsRunnable implements Runnable {
        @Override
        public void run() {
            refreshList();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_check_list, container, false);

        mUnbinder = ButterKnife.bind(this, rootView);

        executor.submit(new LogsRunnable());

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    public void refreshList() {
        final String logs = FileHelper.readFromFile(getContext());
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mChecksList.setText(logs);
            }
        });
    }

}
