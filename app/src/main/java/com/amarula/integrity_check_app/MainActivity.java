package com.amarula.integrity_check_app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import com.amarula.integrity_check_app.util.FileHelper;

/**
 * Main activity of class. Starts {@link CheckLogsTask}, contains {@link CheckListFragment} with
 * parsed informations.
 * <p>
 * Contributors:
 * Peter Janicka (peterj@amarulasolutions.com)
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "IntegrityChecker";

    protected static final String CHECK_LIST_FRAGMENT = "checkListFragment";
    private CheckLogsTask mCheckTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      if (savedInstanceState == null) {
        CheckListFragment fragment = new CheckListFragment();

        getSupportFragmentManager().beginTransaction()
            .add(R.id.container_check_list_fragment, fragment, CHECK_LIST_FRAGMENT)
            .commit();

        mCheckTask = new CheckLogsTask(this);
        mCheckTask.execute();
      }

      FloatingActionButton rebootButton = (FloatingActionButton) findViewById(R.id.fab);
      rebootButton.setOnClickListener(new OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                          PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                                          pm.reboot(null);
                                        }
                                      }
      );
    }

  @Override
  protected void onStart() {
    super.onStart();
    Intent intent = new Intent(this, NotificationIntegrityCheck.class);
    startService(intent);
  }

  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_clear) {
            showClearDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCheckTask.cancel(true);
        mCheckTask.unblockIfNeeded();
    }

    private void showClearDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle(R.string.menu_item_delete)
                .setMessage(R.string.clear_logs_message)
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        FileHelper.deleteFile(getBaseContext());
                        refreshList();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void refreshList() {
        CheckListFragment fragment = (CheckListFragment) getSupportFragmentManager()
                .findFragmentByTag(CHECK_LIST_FRAGMENT);

        if (fragment != null) {
            fragment.refreshList();
        }
    }
}
