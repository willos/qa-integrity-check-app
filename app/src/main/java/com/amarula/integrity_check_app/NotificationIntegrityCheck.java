package com.amarula.integrity_check_app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.widget.Toast;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * @author william.detorvy-ballou
 */

public class NotificationIntegrityCheck extends Service {

  private static final String TAG = "NotifIntegrityCheck";
  private NotificationManager mNM;
  // Unique Identification Number for the Notification.
  // We use it on Notification start, and to cancel it.
  private int NOTIFICATION = R.string.local_service_label;


  @Override
  public void onCreate() {
    mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    // Display a notification about us starting.  We put an icon in the status bar.
    showNotification();
  }

  @Override
  public void onDestroy() {
    // Cancel the persistent notification.
    mNM.cancel(NOTIFICATION);

    // Tell the user we stopped.
    Toast.makeText(this, R.string.local_service_stopped, Toast.LENGTH_SHORT).show();
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  /**
   * Show a notification while this service is running.
   */
  private void showNotification() {

    // The PendingIntent to launch our activity if the user selects this notification
    PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
        new Intent(this, MainActivity.class), 0);

    // Time till next integrity check
    int bar = new BigDecimal(86100 - (SystemClock.uptimeMillis()/1000)).intValueExact();
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.SECOND, +bar);
    Date date = calendar.getTime();

    // Set the info for the views that show in the notification panel.
    Notification notification = new Notification.Builder(getApplicationContext())
        .setSmallIcon(R.mipmap.ic_launcher)  // the status icon
        .setWhen(date.getTime())  // the time stamp System.millis + seconds countdown...
        .setUsesChronometer(true)
        .setContentTitle(getText(R.string.local_service_label))  // the label of the entry
        .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
        .setContentText("Next check: " + date.toString()
            .substring(0, date.toString().length() - 4)) // the contents of the entry
        .setOngoing(true)
        .build();

    // Send the notification.
    mNM.notify(NOTIFICATION, notification);
  }
}
