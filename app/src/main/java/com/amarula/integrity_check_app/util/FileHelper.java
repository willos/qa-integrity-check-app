package com.amarula.integrity_check_app.util;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Helper class for operations with file. Provides methods for saving lines to file and reading
 * the file.
 * <p>
 * Contributors:
 * Peter Janicka (peterj@amarulasolutions.com)
 */

public class FileHelper {
    private static final String TAG = "FileHelper";

    private static final String FILE_NAME = "integrity_check_list.txt";

    public static void saveToFile(Context context, String parsedLog) {
        File file = new File(context.getFilesDir(), FILE_NAME);

        Log.d(TAG, "Saving to file: " + file.getPath());

        OutputStreamWriter out = null;

        try {
            out = new OutputStreamWriter(context.openFileOutput(FILE_NAME, Context.MODE_APPEND));
            out.write(parsedLog + '\n');
        } catch (IOException e) {
            Log.e(TAG, "Error: ", e);
        } finally {
            try {
                if (out != null) out.close();
            } catch (IOException e) {
                Log.e(TAG, "Error: ", e);
            }
        }
    }

    public static String readFromFile(Context context) {
        File file = new File(context.getFilesDir(), FILE_NAME);

        StringBuilder content = new StringBuilder();

        if (!file.exists()) {
            Log.w(TAG, "File with integrity checks not found");
            return content.toString();
        }

        InputStreamReader inputreader = null;
        try {
            inputreader = new InputStreamReader(context.openFileInput(FILE_NAME));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Error: ", e);
        }
        BufferedReader buffreader = new BufferedReader(inputreader);

        try {
            String line;
            while ((line = buffreader.readLine()) != null) {
                content.append(line + '\n');
            }

            return content.toString();
        } catch (IOException e) {
            Log.e(TAG, "Error: ", e);
        } finally {
            try {
                inputreader.close();
                buffreader.close();
            } catch (IOException e) {
                Log.e(TAG, "Error: ", e);
            }
        }

        return content.toString();
    }

    public static void deleteFile(Context context) {
        File file = new File(context.getFilesDir(), FILE_NAME);

        if (file.exists()) {
            file.delete();
        }
    }
}
